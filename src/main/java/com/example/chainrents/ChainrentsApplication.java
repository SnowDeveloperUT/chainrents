package com.example.chainrents;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChainrentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChainrentsApplication.class, args);
	}
}
