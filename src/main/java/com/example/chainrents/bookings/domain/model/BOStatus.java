package com.example.chainrents.bookings.domain.model;

/**
 * Created by snowwhite on 6/9/2017.
 */
public enum BOStatus {
	CREATED, PENDING, OPEN, REJECTED, CLOSED, CANCELLED
}
