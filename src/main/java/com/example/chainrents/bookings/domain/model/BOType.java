package com.example.chainrents.bookings.domain.model;

/**
 * Created by snowwhite on 6/9/2017.
 */

public enum BOType {
	CUSTOMER, AGENCY
}
