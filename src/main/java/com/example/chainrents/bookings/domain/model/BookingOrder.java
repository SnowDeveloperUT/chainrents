package com.example.chainrents.bookings.domain.model;

import com.example.chainrents.common.domain.model.BusinessPeriod;
import com.example.chainrents.properties.domain.model.PropertyReservation;
import com.example.chainrents.properties.domain.model.RentProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/9/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class BookingOrder {

	@Id
	String id;

	@ManyToOne
	RentProperty property;
	@Embedded
	BusinessPeriod bookingPeriod;

	@OneToOne
	PropertyReservation roomReservation;


	@Enumerated(EnumType.STRING)
	BOStatus status;
	@Enumerated(EnumType.STRING)
	BOType type;

	@Column(precision = 8, scale = 2)
	BigDecimal total;

}
