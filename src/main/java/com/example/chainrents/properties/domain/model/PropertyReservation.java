package com.example.chainrents.properties.domain.model;

import com.example.chainrents.common.domain.model.BusinessPeriod;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by snowwhite on 6/9/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class PropertyReservation {

	@Id
	String id;
	@ManyToOne
	RentProperty property;
	@Embedded
	BusinessPeriod period;

}
