package com.example.chainrents.properties.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/9/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class RentProperty {

	@Id
	String id;
	@Enumerated(EnumType.STRING)
	RentPropertyType propertyType;
	BigDecimal bitcoinPricePerDay;
	String streetAddress;
	String city;
	BigDecimal aream2;
	int floorNum;
	String amenities;
	String services;
}
