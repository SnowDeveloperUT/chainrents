package com.example.chainrents.properties.domain.model;

/**
 * Created by snowwhite on 6/9/2017.
 */
public enum RentPropertyType {
	APARTMENT, HOUSE, SINGLEROOM, SHAREDROOM
}