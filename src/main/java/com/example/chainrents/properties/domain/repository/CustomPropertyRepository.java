package com.example.chainrents.properties.domain.model.properties.domain.repository;



import com.example.chainrents.properties.domain.model.RentProperty;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by snowwhite on 6/9/2017.
 */
public interface CustomPropertyRepository {
	List<RentProperty> findAvailable(LocalDate startDate, LocalDate endDate);
	boolean isRoomAvailable(RentProperty room, LocalDate startDate, LocalDate endDate);
}
