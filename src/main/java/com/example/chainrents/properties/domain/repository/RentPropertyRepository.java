package com.example.chainrents.properties.domain.model.properties.domain.repository;

import com.example.chainrents.properties.domain.model.RentProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/9/2017.
 */
@Repository
public interface RentPropertyRepository extends JpaRepository<RentProperty, String>, CustomPropertyRepository {
}
