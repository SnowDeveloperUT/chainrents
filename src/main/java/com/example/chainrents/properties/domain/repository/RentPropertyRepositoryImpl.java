package com.example.chainrents.properties.domain.model.properties.domain.repository;


import com.example.chainrents.properties.domain.model.RentProperty;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by snowwhite on 6/9/2017.
 */
@SuppressWarnings("JpaQlInspection")
public class RentPropertyRepositoryImpl implements CustomPropertyRepository {

	@Autowired
	EntityManager em;

	@Override
	public List<RentProperty> findAvailable(LocalDate startDate, LocalDate endDate) {
		TypedQuery<RentProperty> query = em.createQuery(
				"select r from rent_property r where r not in (select r.rent_property from PropertyReservation r where ?1 < r.period.endDate and ?2 > r.period.startDate)"
				, RentProperty.class)
				.setParameter(1, startDate)
				.setParameter(2, endDate);
		return query.getResultList();
	}

	@Override
	public boolean isRoomAvailable(RentProperty room, LocalDate startDate, LocalDate endDate) {
		TypedQuery<RentProperty> query = em.createQuery(
				"select r from rent_property r where r = ?3 and r not in (select r.room from PropertyReservation r where ?1 < r.period.endDate and ?2 > r.period.startDate)"
				, RentProperty.class)
				.setParameter(1, startDate)
				.setParameter(2, endDate)
				.setParameter(3, room);
		return query.getResultList().size() > 0;
	}


}
