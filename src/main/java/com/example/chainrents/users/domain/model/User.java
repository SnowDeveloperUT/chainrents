package com.example.chainrents.users.domain.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by snowwhite on 6/9/2017.
 */
@Data
@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class User {
	@Id
	String id;
	String username;
	String address;
	String email;
}
