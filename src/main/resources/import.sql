insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (1, 'APARTMENT', 100, 'Raatuse22', 'Tartu', 100, 2, 'Security', 'AC, TV');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (2, 'APARTMENT', 200, 'Salkia', 'Kol', 200, 3, 'Water', 'AC, Geyser');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (3, 'APARTMENT', 300, 'Wakad', 'Pune', 150, 4, 'Security', 'AC, TV');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (4, 'APARTMENT', 150, 'NCR', 'Delhi', 150, 2, 'Security', 'AC, Geyser');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (5, 'APARTMENT', 150, 'Sidisaba', 'Tallinn', 300, 2, 'Water', 'AC, TV');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (6, 'APARTMENT', 150, 'Vihar', 'Goa', 180, 5, 'Security', 'AC, TV');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (7, 'APARTMENT', 130, 'Latin', 'Moscow', 400, 7, 'Security', 'AC, Geyser');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (8, 'APARTMENT', 120, 'Liluah', 'Howrah', 550, 1, 'Water', 'AC, TV');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (9, 'APARTMENT', 150, 'Kusto', 'Riga', 700, 1, 'Security', 'AC, Geyser');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (10, 'APARTMENT', 300, 'Pinte', 'Helsinki', 460, 2, 'Security', 'AC, Geyser');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (11, 'APARTMENT', 100, 'Andrew Street', 'New York', 660, 6, 'Security', 'AC, TV');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (12, 'APARTMENT', 500, 'Voru', 'Tartu', 230, 8, 'Water', 'AC, TV');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (13, 'APARTMENT', 600, 'Estonia', 'Tartu', 450, 9, 'Security', 'AC, Geyser');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (14, 'APARTMENT', 700, 'Estonia', 'Tallinn', 670, 1, 'Water', 'AC, TV');
insert into rent_property (id, property_type, bitcoin_price_per_day, street_Address, city, aream2, floor_num, amenities, services) values (15, 'APARTMENT', 100, 'Raatuse24', 'Tartu', 60, 2, 'Security', 'AC, Geyser');



insert into property_reservation (id, property_id, start_date, end_date) values (1, 1, '2017-04-22', '2017-07-26');
insert into property_reservation (id, property_id, start_date, end_date) values (2, 4, '2017-04-22', '2017-07-26');
insert into property_reservation (id, property_id, start_date, end_date) values (3, 9, '2017-04-22', '2017-07-26');
insert into property_reservation (id, property_id, start_date, end_date) values (4, 10, '2017-04-22', '2017-04-26');
insert into property_reservation (id, property_id, start_date, end_date) values (5, 15, '2017-04-22', '2017-04-26');
insert into property_reservation (id, property_id, start_date, end_date) values (6, 5, '2017-04-22', '2017-04-26');
insert into property_reservation (id, property_id, start_date, end_date) values (7, 3, '2017-04-22', '2017-04-26');
insert into property_reservation (id, property_id, start_date, end_date) values (8, 1, '2017-04-22', '2017-04-26');

insert into user (id, username, address, email) values (1, 'abc123', 'Kolkata', 'abc@gmail.com');
insert into user (id, username, address, email) values (2, 'snowwhite', 'Rome', 'snowwhite@gmail.com');
insert into user (id, username, address, email) values (3, 'princess', 'Pune', 'princess@gmail.com');
insert into user (id, username, address, email) values (4, 'rose', 'Goa', 'rose@gmail.com');